<?php
require __DIR__ . '/../php_util/db_connection.php';
session_start();
$mysqli = get_db_connection_or_die();
$user_id = $_SESSION['user_id'];
$id = $_GET['id']
?>
<!DOCTYPE html>

<head>
	<meta charset="UTF8">
	<title>Pagina</title>
	<!--link estilos css-->
	<link rel="stylesheet" type="text/css" href="/static/style.css" />
</head>

<body>
	<!--Imagen fondo-->
	<div class="fondo">
		<img class="img_fondo" src="/static/fondo.jpg" alt="Fondo" width="100%" height="100%">
	</div>
	<!--Boton logout-->

	<!--Logo-->
	<div class="logo1">
		<img src="/static/logo.png" alt="logo" height="100px" width="100px">
	</div>
	<!--Contenido PHP-->
	<div class="container3">
		<div class="boton_logout">
			<button onclick="window.location.href='/logout.php'">Logout</button>
		</div>
		<?php
		//Comprobamos que la sesión no está vacía
		if (empty($user_id) || empty($id) ) {
			header('Location: error.php?mensaje=Error');
		} else {
			echo '<br>';
			echo '<br>';
			echo '<br>';
			echo '<br>';
			echo '<h1>Ofertas Para tu evento</h1>';
			// guardamos las ofertas no aceptadas con el id del evento
			$query = 'SELECT tOffer.id, tOffer.total_price, tUser.business_name FROM tOffer INNER JOIN tUser ON tOffer.bussiness_user_id=tUser.id WHERE tOffer.event_id = ' . $id . ' AND tOffer.is_accepted = 0';
			$result1 = mysqli_query($mysqli, $query) or die('query error');
			if (mysqli_affected_rows($mysqli) == 0) {
				echo '<p> No Hay Ofertas para tu evento</p>';
			} else {
				echo '<table class="default">';
				echo '<tr>';
				echo '<th>NOMBRE DEL NEGOCIO</th>';
				echo '<th>PRECIO DE LA OFERTA</th>';
				echo '<th>VER OFERTA</th>';
				echo '</tr>';
				// Generamos una tabla con los datos 
				while ($row = mysqli_fetch_array($result1)) {
					echo '<tr>';
					echo '<td>' . $row[2] . '</td>';
					echo '<td>' . $row[1] . '</td>';
					//link para poder ir a la pagina de la oferta para aceptarla
					echo '<td><a href="aceptar_oferta.php?id=' . $row['id'] . '">Ver oferta</td>';
					echo '</tr>';
				}

				echo '</table>';

				mysqli_close($mysqli);
			}
		}
		?>

	</div>
</body>

</html>