<?php
ini_set('display_errors', 'On');
require __DIR__ . '/../php_util/db_connection.php';

session_start();
$mysqli = get_db_connection_or_die();
//recogemos los parametros.
$bussiness_id = $_SESSION['user_id'];
$oferta_descripcion = $_POST['ofertaDescripcion'];
$oferta_precio = $_POST['ofertaPrecio'];
$event_id = $_POST['evento_id'];

//definimos is_accepted a false.
$is_accepted = intval(false);

//Consulta que inserta en la tabla tOffer la oferta del usuario bussiness.
try {
    $stmt = $mysqli->prepare("INSERT INTO tOffer (total_price, extra_info, bussiness_user_id , event_id, is_accepted) 
        VALUES (?, ?, ?, ?, ?)");

    $stmt->bind_param("isiii", $oferta_precio, $oferta_descripcion, $bussiness_id, $event_id, $is_accepted);
    $stmt->execute();
    header('Location: main.php');
    $stmt->close();
    
} catch (Exception $e) {
    error_log($e);
    header('Location: offer.php?failed=True');
}
$mysqli->close();
