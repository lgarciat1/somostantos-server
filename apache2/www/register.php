<!DOCTYPE html>
<html>

<head>
  <meta charset="UTF8">
  <link rel="stylesheet" type="text/css" href="/static/style.css" />
</head>


<body>

  <div class="logo"><img src="/static/LOGOSMALL.png" alt="logo"></div>
  <div class="fondo"><img src="/static/imagen2.jpg" alt="fondo" width="100%" height="auto"></div>
  <div class="container2">
    <h1>REGISTRO</h1>

    <?php
    if (isset($_GET['success'])) {
      if ($_GET['success'] == TRUE) {
        echo "<p>BRAVO! Nuevo usuario SEGURISIMO registrado con éxito 100% garantizado NO FAKE </p>";
      }
    }
    if (isset($_GET['register_failed_email'])) {
      if ($_GET['register_failed_email'] == TRUE) {
        echo ("<p>Email ya registrado en base de datos</p>");
      }
    }
    if (isset($_GET['register_failed_unknown'])) {
      if ($_GET['register_failed_unknown'] == TRUE) {
        echo ("<p>Fallo en el registro</p>");
      }
    }


    ?>

    <form method="post" action="/do_register.php">

      <div class="row">
        <div class="col-25">

          <label>Nombre</label>

        </div>
        <div class="col-75">

          <input id="name" name="name" type="text"></input>

        </div>
      </div>


      <div class="row">
        <div class="col-25">

          <label>Apellido</label>
        </div>

        <div class="col-75">
          <input id="surname" name="surname" type="text"></input>
        </div>
      </div>

      <div class="row">
        <div class="col-25">

          <label>e-mail</label>

        </div>
        <div class="col-75">


          <input id="email" name="email" type="text"></input>
        </div>
      </div>



      <div class="row">
        <div class="col-25">

          <label>Contraseña</label>

        </div>
        <div class="col-75">

          <input id="pass" name="pass" type="password"></input>

        </div>
      </div>

      <div class="row">
        <div class="col-25">

          <label>Repite contraseña</label>
        </div>
        <div class="col-75">


          <input id="pass2" name="pass2" type="password"></input>
        </div>
      </div>



      <div class="row">
        <div class="col-75">
          <label>(Cubre estos campos si quieres registrarte como negocio)</label>
        </div>
      </div>

      <div class="row">
        <div class="col-25">
          <label>Nombre del negocio</label>

        </div>
        <div class="col-75">
          <input id="business_name" name="business_name" type="text"></input>
        </div>
      </div>





      <div class="row">
        <div class="col-25">
          <label>Latitud del negocio</label>
        </div>
        <div class="col-75">

          <input id="business_latitude" name="business_latitude" type="number" step="0.000001" max="180" min="-180"></input>
        </div>
      </div>
      <div class="row">
        <div class="col-25">
          <label>Longitud del negocio</label>
        </div>
        <div class="col-75">
          <input id="business_longitude" name="business_longitude" type="number" step="0.000001" max="180" min="-180"></input>
        </div>
      </div>
      <div class="row botones">
        <input class="register" type="submit" value="Enviar">
        <button type="button" onclick="window.location.href='/login.php'">Login</button>

      </div>

    </form>
  </div>

</body>

</html>