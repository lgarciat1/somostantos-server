<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    
      <link rel="StyleSheet" href="/static/style.css" type="text/css" media=screen>
 
</head>
<body>
    <div class="logo"><img src="/static/logo.png" alt=""></div>
    <div class="fondo"><img src="/static/imagen2.jpg" alt="" width="100%"></div>
    <div class="container2">
    <?php
      // recoger la variable $_GET['failed'] para mostrar el error
      if ($_GET['failed'] == 'True'){
         echo ('<p class = "alerta" >La inserción ha fallado. Vuelve a intentarlo</p>');
      }
      
    ?>  
    <h1>Crear Evento</h1>
    <?php
      // conexión a la bbdd
     // ini_set('display_errors', 'On'); // Something useful!
      require __DIR__ . '/../php_util/db_connection.php';
      session_start();
      $mysqli = get_db_connection_or_die();
      $user_id = $_SESSION['user_id'];
      // se comprueba que hay un id de usuario
      if (empty($user_id)){
        header("Location: error.php?mensaje=El usuario no ha iniciado la sesión");
      }else{
   
      //consulta a la base de datos para saber el tipo de usuario
        try{
          $query = "SELECT * FROM tUser WHERE id = ".$user_id;
          $result = mysqli_query($mysqli, $query) or die('Query error');
          $only_row = mysqli_fetch_array($result);
          //si es usuario businnes no permite registrar eventos
            if ($only_row['business_name'] !== NULL){
                die('Usuario tipo Empresa no puede registrar eventos');
            }else{
           
      ?>
      <!-- formulario de entrada de datos-->
              <form id = "formulario" method="post" action="/do_create_event.php">
                <div class="row">
                  <div class="col-25">
                    <label for="nombre">Nombre</label>
                  </div>
                  <div class="col-75">
                  <input type="text" id="nombre" name="nombre" readonly value= <?php echo $only_row['name'] ?> > 
                  
                  </div>
                </div>
                <div class="row">
                  <div class="col-25">
                    <label for="fecha">Fecha y hora</label>

                    <?php 
                    // recoger la variable $_GET['failed_date'] para mostrar el error
                      if ($_GET['failed_date'] == 'True'){
                          echo ('<p class ="alerta">Fecha no válida</p>');    } 
                    ?>

                  </div>
                 
                  <div class="col-75">
                    <input type="datetime-local" id="fecha" name="fecha"  required>
                  </div>
                </div>
          
                <div class="row">
                  <div class="col-25">
                    <label for="numComensales">Numero Comensales</label>
                  </div>
                  <div class="col-75">
                    <input type="number" id="numComensales" name="numComensales"  min="1" pattern="^[0-9]+" required>
                  </div>
                </div>
                <div class="row botones">
                  <input class="register" type="submit" value="Crear evento">
                  
                </div>
              </form>
          <?php
            }
   
        } catch(Exception $e){
          error_log($e);
	  console.log($e);
        }
      }
    ?>   
 
  </div>
  <!--script que obtiene los datos de latitud y longitud y crea inputs no visibles con esos datos-->
  <script src="geolocalizacion.js"></script>
 
</body> 
</html>

