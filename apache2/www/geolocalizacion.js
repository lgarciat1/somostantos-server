// Al cargar el documento lanzamos la función
document.addEventListener("DOMContentLoaded", function () {
    getLocation();
  })
  
  // Si funciona la geolocalización y nos dan acceso enviamos a showPosition()
  function getLocation() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(showPosition);
    }
  }
  // Apendicamos al form las coordenadas del dispositivo
  function showPosition(position) {
    let form = document.getElementById("formulario");
    let hiddenlatitude = document.createElement('input');
    hiddenlatitude.setAttribute("type", "hidden");
    hiddenlatitude.setAttribute("name", "lat");
    hiddenlatitude.setAttribute("value", position.coords.latitude);
    let hiddenlongitude = document.createElement('input');
    hiddenlongitude.setAttribute("type", "hidden");
    hiddenlongitude.setAttribute("name", "lon");
    hiddenlongitude.setAttribute("value", position.coords.longitude);
    form.appendChild(hiddenlatitude);
    form.appendChild(hiddenlongitude);
  }