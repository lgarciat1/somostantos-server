from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
from .models import Tuser
import json
import random
import string
import bcrypt

#Método para permitir el registro de nuevos usuarios
@csrf_exempt
def register(request):
    if request.method !='POST':
        return JsonResponse(status=405, data={"Error": "Método no permitido"})

    json_body = json.loads(request.body)
    
    empresa=True
    try:
        uname = json_body['name']
        usurname=json_body['surname']
        uemail=json_body['email']
        upassword=json_body['password']
    except KeyError:
        return JsonResponse(status=400, data={"Error": "Alguno de los parámetros no es válido o falta"})
    
    try:
        Tuser.objects.get(email=uemail)
        return JsonResponse(status=409, data={"Error": "Ya existe un usuario registrado con ese email"})
    except Tuser.DoesNotExist:
        try:
            ubusiness= json_body['business']
        except KeyError:
            empresa=False

    uencrypted_password = bcrypt.hashpw(upassword.encode('utf8'), bcrypt.gensalt()).decode('utf8')

    if empresa:
        try:
            ubusinessname=ubusiness['name']
            ubusinesslongitude=ubusiness['longitude']
            ubusinesslatitude=ubusiness['latitude']
        except KeyError:
            return JsonResponse(status=400, data={"Error": "Alguno de los parámetros no es válido o falta"})
        user = Tuser(name=uname, surname=usurname, email=uemail, encrypted_password=uencrypted_password, business_name=ubusinessname, business_longitude=ubusinesslongitude, business_latitude=ubusinesslatitude)
    else:
        user = Tuser(name=uname, surname=usurname, email=uemail, encrypted_password=uencrypted_password)

    user.save()
    return JsonResponse(status=201, data={"OK": "El usuario ha sido creado correctamente"})

#Método para generar el token de la sesión
def generarToken():
    valores = string.ascii_letters + string.digits
    token = ''.join((random.choice(valores) for i in range(20)))
    return token

#Método para permitir el inicio de sesión de los usuarios
@csrf_exempt
def login(request):
    if request.method !='POST':
        return JsonResponse(status=405, data={"Error": "Método no permitido"})

    json_body = json.loads(request.body)

    try:
        uemail=json_body['email']
        upassword=json_body['password']
    except KeyError:
        return JsonResponse(status=400, data={"Error": "Falta alguno de los parámetros"})

    try:
        bbdd=Tuser.objects.get(email=uemail)
        if bcrypt.checkpw(upassword.encode('utf8'),bbdd.encrypted_password.encode('utf8')) == False:
            return JsonResponse(status=401, data={"Error": "Contraseña incorrecta"})
    except Tuser.DoesNotExist:
        return JsonResponse(status=404, data={"Error": "El usuario no existe"})

    bbdd.active_session_token=generarToken()
    bbdd.save()

    business=True
    if bbdd.business_name == None:
        business=False
    
    return JsonResponse(status=201, data={
        "user_id":bbdd.id,
        "name":bbdd.name,
        "is_business": business,
        "token":bbdd.active_session_token
    })
