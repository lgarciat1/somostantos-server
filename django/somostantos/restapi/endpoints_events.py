from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from .models  import Tuser, Toffer, Tevent
import math
import json

@csrf_exempt
def events(request):
    try:
        token = request.headers['token']    
    except KeyError:
        return JsonResponse(status=401,data={'error':'La petición ha fallado porque no se ha enviado el token de autenticación en las cabeceras'})  
        
    try:
        user = Tuser.objects.get(active_session_token = token)
    except Tuser.DoesNotExist:    
        return JsonResponse(status=403,data={'error':'La petición ha fallado porque el token de autenticación no es válido'})

    if request.method == 'GET':
    
        author_user_id = request.GET.get('author_user_id')
        
        # If client app request 'events - by - author_id'
        if author_user_id is not None:
            events = Tevent.objects.filter(author=user)
            respuesta = []
            for event in events:
                diccionario = {}
                diccionario['id'] = event.id
                diccionario['author_name'] = event.author.name
                diccionario['datetime'] = event.datetime
                diccionario['number_attendants'] = event.number_attendants
                diccionario['latitude'] = event.latitude
                diccionario['longitude'] = event.longitude
                respuesta.append(diccionario)
            return JsonResponse(respuesta, safe=False, status=200)
            
        # Otherwise, let's check for latitude/longitude/radius
        try:
            latitude =  request.GET['latitude']
            longitude =  request.GET['longitude']
            radius1 =  request.GET['radius']
            is_open = request.GET['is_open']
        except KeyError:
            return JsonResponse(status=400,data={'error':'La petición ha fallado porque falta alguno de los parámetros o es inválido'})
        if is_open != "true":
            return JsonResponse(status=400,data={"error":"IS OPEN IS NOT TRUE"})
            
        events = Tevent.objects.all()

        latitud_param =  request.GET['latitude']
        longitude_param =  request.GET['longitude']
        radius =  request.GET['radius']
        respuesta = []
        radius_earth = 6373.0

        for event in events: 
            # TO-DO: Comprobar que event no tiene ninguna offer con is_accepted = True.
            #        En caso de que la tenga, NO deberíamos devolverla
            lat1 = math.radians(event.latitude)
            lon1 = math.radians(event.longitude)
            lat2 = math.radians(float(latitud_param))
            lon2 = math.radians(float(longitude_param))
            
            dlon = lon2 - lon1
            dlat = lat2 - lat1

            a = math.sin(dlat / 2)**2 + math.cos(lat1) * math.cos(lat2) * math.sin(dlon / 2)**2
            c = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a))

            distance = radius_earth * c
            print(distance)

            if distance > float(radius):
                continue  
            diccionario = {}
            diccionario['id'] = event.id
            diccionario['author_name'] = event.author.name
            diccionario['datetime'] = event.datetime
            diccionario['number_attendants'] = event.number_attendants
            diccionario['latitude'] = event.latitude
            diccionario['longitude'] = event.longitude
            respuesta.append(diccionario)
        return JsonResponse(respuesta, safe=False, status=200)
    elif request.method == 'POST':
        json_body = json.loads(request.body)
        event = Tevent()
        event.datetime = json_body['datetime']
        event.number_attendants = json_body['number_attendants']
        event.latitude = json_body['latitude']
        event.longitude = json_body['longitude']
        event.author = user
        event.save()
        return JsonResponse(status=201, data={"Creado": "Con éxito"})
    else:
        return JsonResponse(status=405,data={'error':405})
