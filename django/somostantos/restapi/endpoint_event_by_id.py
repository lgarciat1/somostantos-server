import json

from django.http import JsonResponse

from .models import Tevent, Tuser, Toffer


def event_by_id(request,id_sent):
	if request.method !='GET':
		return  JsonResponse(status=405, data = {'message':' La petición enviada no se corresponde con el método GET'})

	try:
		event = Tevent.objects.get(id = id_sent)
		offert = Toffer.objects.all()

	except Tevent.DoesNotExist:
		return JsonResponse(status=404, data={'message':'No se ha encontrado ningun evento con el ID intrducido '})


	try:
		sessionToken= request.headers['token']

	except KeyError:
		return JsonResponse(status=401, data={'message': 'No se ha introducido o enviado un token de autentificación en la cabecera '})

	try:
		bussiness_user_id = Tuser.objects.get(active_session_token = sessionToken)

	except Tuser.DoesNotExist:
		return JsonResponse(status=403,data= { 'message':' El token de sesión no es válido '})


	event = Tevent.objects.get(id = id_sent)
	offert = Toffer.objects.filter(event_id = id_sent)
	list_offer =[]

	for filaOffer in offert:
		joker = {}
		joker['offer_id']= filaOffer.id
		joker['bussiness_name']=filaOffer.bussiness_user.business_name
		joker['latitude']=filaOffer.bussiness_user.business_latitude
		joker['longitud']=filaOffer.bussiness_user.business_longitude
		joker['total_price']=filaOffer.total_price
		joker['extra_info']=filaOffer.extra_info
		joker['is_accepted']=filaOffer.is_accepted
		list_offer.append(joker)
		print(joker)

	print(list_offer)
	resultado = {
		'author_name': event.author.name,
		'datetime': event.datetime,
		'number_attendants': event.number_attendants,
		'latitude': event.latitude,
		'longitude': event.longitude,
		'offert':list_offer
	}

	return JsonResponse(status=200, data = resultado)
